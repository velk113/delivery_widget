# __DeliveryWidget__


Виджет предназначен для отображения актуальной информации о пунктах самовывоза СДЭК с привязкой их к карте, возможностью расчета доставки (сроков и стоимости) для указанных габаритов товаров. [Демо]('https://bitbucket.org/velk113/delivery_widget_example/src/master/').

## __Инсталляция__:


### 
Переместить django app в ваш проект, и добавить его в `INSTALLED_APPS` в файле `settings.py` вашего проекта:

```python
INSTALLED_APPS = [
    ...
    'delivery_widget',
    ...
]
```


Также добавить найстройки для cdek api:
```python
CDEK_API = {
    'CDEK_ACCOUNT': 'EMscd6r9JnFiQ3bLoyjJY6eM78JrJceI',
    'CDEK_SECURE_PASSWORD': 'PjLZkKBHEiLK3YsjtNrt3TGNG0ahs3kG',
    'CDEK_TARIFF_CODE': '10'
    'CODE_SENDING_LOCATION': '137',
    'IS_SANDBOX': True,
}
```
- `CDEK_ACCOUNT` - идентификатор клиента.

- `CDEK_SECURE_PASSWORD` - секретный ключ клиента.

- `CDEK_TARIFF_CODE` - Код тарифа ([подробнее](https://confluence.cdek.ru/pages/viewpage.action?pageId=63345430#id-%D0%9A%D0%B0%D0%BB%D1%8C%D0%BA%D1%83%D0%BB%D1%8F%D1%82%D0%BE%D1%80%D0%A0%D0%B0%D1%81%D1%87%D0%B5%D1%82%D0%BF%D0%BE%D0%BA%D0%BE%D0%B4%D1%83%D1%82%D0%B0%D1%80%D0%B8%D1%84%D0%B0-calc_tariff1)).

- `CDEK_SENDING_LOCATION` - Код cdek города отправки.

- `IS_SANDBOX` - True есле работаете под тестовым аккаунтом cdek.



В корневой `urls.py` требуется добавить:
```python
from django.urls import path, include

urlpatterns = [
    ...
    path('delivery_widget/', include('delivery_widget.urls')),
    ...
]
```


На странице подключить api яндекс карт, для него требуется ключ получить который можно здесь [кабинет разработчика](https://developer.tech.yandex.ru/). А также сам виджет:
```html
<script src="https://api-maps.yandex.ru/2.1/?apikey=ВАШ API КЛЮЧ&lang=ru_RU" type="text/javascript"></script>
<script src='{% static 'delivery_widget.js' %}'></script>
```

Создать контейнер для виджета:
```html
<div id='myDeliveryWidget' style="width=600px; height: 400px;">
```
Проиницилизировать виджет:
```js
ymaps.ready(init);

function init() {

    const delivery_widget = new DeliveryWidget('myDeliveryWidget', {
        path: 'http://127.0.0.1:8000/delivery_widget/',
        csrfToken: csrfToken,    
        minLoadZoom: 10,  
        zoom: 10,
        center: [55.76, 37.64],
        gridSize: 70,
        choose: true,
        packages: [
            { length: 20, width: 20, height: 20, weight: 2 },
            { length: 20, width: 20, height: 20, weight: 2 },
        ],
        onPickDeleveryPoint: (data) => {
            console.log(data);
        }      
    }
}
```
__Параметры__:
- `path` - путь к app.
- `miLoadZoom` -  с какого маштаба, начинать подгрузку ПВЗ.
- `zoom` - коэффициент масштабирования.
- `center` - центр карты, есле не задать центром будет местоположение пользователя.
- `gridSize` - размер ячейки кластеризации в пикселях.
- `choose` - отображение кнопки выбора ПВЗ.
- `packages` - установка данных о товаре, есле не задано стоимость рассчитываться не будет.
- `onPickDeleveryPoint` - установка callback функции, на событие выбора ПВЗ.



## __DeliveryPoints__


Данные которые принимает виджет от сервера.


No| Название поля | Описание | Тип | Обяз. для заполн.
---|--- | --- | --- | --- 
1 | company | компания доставки | string | Да
2 | company_name | название компании | string | Да
3 | code | cdek код ПВЗ | string | Да
4 | name | Название | string | Да
5 | longtitude | Координаты местоположения (долгота) в градусах | float | Да
6 | latitude | Координаты местоположения (широта) в градусах | float | Да
7 | region | Название региона | string | Да
8 | city | Название города | string | Да
9 | city_code | Код города по базе СДЭК | integer | Да
10 | work_time | Режим работы, строка вида «пн-пт 9-18, сб 9-16» | string | Да
11 | email | Адрес электронной почты | string | Нет
12 | phones | Телефоны пвз | phones[] | Нет
12.1 | number | Номер телефона | string | Нет
13 | cdek_owner_code | cdek или InPost | string | Нет
14 | is_dressing_room | примерочная | boolean | Да
15 | have_cashless | наличие терминала оплаты | boolean | Да
16 | have_cash | наличные | boolean | Да
17 | allowed_cod | наложенный платеж | boolean | Да
18 | office_image_list | компания доставки | office_image_list[] | Нет
18.1 | url | компания доставки | string | Нет



###  __Пример:__
```json
[
    {
        "company":"cdek",
        "company_name":"СДЭК",
        "code": "RZN19",
        "name": "6131 Постамат ОМНИСДЭК",
        "longtitude": 39.71554,
        "latitude": 54.602867,
        "region": "Рязанская обл.",
        "city": "Рязань",
        "city_code": "159",
        "address": "ул. Гоголя, 40",
        "postal_code": 3223,
        "work_time": "Пн-Пт 10:00-20:00, Сб-Вс 10:00-18:00",
        "email": "yola@cdek.ru",
        "phones": [
            {"number": "+74951459645"},
        ],
        "cdek_owner_code": "cdek",
        "is_dressing_room": true,
        "have_cashless": true,
        "have_cash": true, 
        "allowed_cod": true,
        "office_image_list": [
            {"url": "https://pvzimage.cdek.ru/images/5434/12378_IMG_6526.JPG"},
            {"url": "https://pvzimage.cdek.ru/images/5434/12380_IMG_6527.JPG"},
        ]
    },
]
```


## __Методы виджета__



### `void setPackages(packages)` - установка мест (упаковок).
```js
delivery_widget.setPackages([
    { length: 20, width: 20, height: 20, weight: 2 },
    { length: 20, width: 20, height: 20, weight: 2 },
])
```



### `void addPackage(package)` - добавить место(упаковку).
```js
delivery_widget.addPackage({
    length: 20,
    width: 20,
    height: 20,
    weight: 2,
})
```



### `int getCountDeliveryPointsLoaded()` - возращает количество загруженных ПВЗ.



### `array getPackages()` - возращает места (упаковки).



### `void clearPackages()` - удалить все упаковки.



## __События виджета__


### `onPickDeleveryPoint` - событие выбора ПВЗ.
```js
const delivery_widget = new DeliveryWidget('myDeliveryWidget', {
    ...
    onPickDeleveryPoint = showPickedDeleveryPoint,
    ...
}

function showPickedDeleveryPoint(data) {
    console.log(data);
}
```
__Cодержание data:__
```js
data = {
    company: "cdek",
    hintContent: "СДЭК",
    company_name: "СДЭК",
    code: "KEM15",
    city: "Кемерово",
    city_code: "272",
    address: "Б-р Строителей, 28/1",
    work_time: "Пн-Пт 10:00-20:00, Сб-Вс 10:00-18:00",
    have_cash: true,
    have_cashless: true,
    is_dressing_room: true,
    choose:true,
    shippingCost: {
        company: "cdek",
        company_name:"СДЭК",
        currency:"RUB",
        delivery_sum: 580,
        period_min: 1,
        period_max: 1,
        weight_calc: 6500,
        total_sum: 580,
    },
    office_image_list: [
        {"url":"https://pvzimage.cdek.ru/images/4582/7971_5.JPG"},
        {"url":"https://pvzimage.cdek.ru/images/4582/7967_3.JPG"},
    ],
}
``` 

